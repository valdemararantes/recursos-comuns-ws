package br.com.galgo.testes.utils;

import br.com.galgo.testes.constants.MensagemErro;
import br.com.galgo.testes.recursos_comuns.config.ConfiguracaoSistema;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesWS;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlTestSuite;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestStep;
import com.eviware.soapui.model.testsuite.TestStepResult.TestStepStatus;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class TesteWSUtils {

    private static final Logger log = LoggerFactory.getLogger(TesteWSUtils.class);

    public static void gerarRelatorio(final WsdlTestRequestStepResult runTestStep, final String testStepName) {
        geraRelatorioTarget(runTestStep, testStepName);
    }

    public static String getProjeto(Ambiente ambiente) {

        String userPath = System.getProperty(ConstantesWS.USER_HOME);

        final String pathProjetoWsProducao = ambiente.getPathProjetoWs();

        File f = new File(userPath, File.separator + ConstantesWS.CONFIG + File.separator + pathProjetoWsProducao);
        String executavelPath = f.getAbsolutePath();
        return executavelPath;
    }

    public static WsdlTestCase getTestCase(String nomeSuite, String nomeTestCase, Ambiente ambiente)
            throws ErroAplicacao {
        final String projeto = getProjeto(ambiente);
        try {
            WsdlProject project = new WsdlProject(projeto);
            WsdlTestSuite testSuite = project.getTestSuiteByName(nomeSuite);
            log.debug("testeSuite: ", testSuite.getLabel());
            log.debug("testCase: ", nomeTestCase);
            return testSuite.getTestCaseByName(nomeTestCase);
        } catch (Exception e) {
            throw new ErroAplicacao(MensagemErro.ERRO_CARREGAR_XML + projeto);
        }
    }

    public static WsdlTestStep getTestStep(WsdlTestCase testCase, String nomeTestStep) throws ErroAplicacao {
        return testCase.getTestStepByName(nomeTestStep);
    }

    public static void validaTestStep(final WsdlTestRequestStepResult runTestStep) throws IOException {

        final TestStepStatus testStepStatus = runTestStep.getStatus();
        try {
            Assert.assertEquals(TestStepStatus.OK, testStepStatus);
        } catch (AssertionError e) {
            log.error("TestStepStatus = {}", testStepStatus);
            Assert.fail(MensagemErro.STATUS_INVALIDO);
        } catch (Exception e) {
            Assert.fail(MensagemErro.ERRO_GENERICO);
        }
    }

    private static void criaCaminhoEvidencia(final String caminhoEvidencia) throws IOException {
        final String path = FilenameUtils.getFullPathNoEndSeparator(caminhoEvidencia);
        File file = new File(path);
        if (!file.exists()) {
            FileUtils.forceMkdir(file);
        }
    }

    private static void geraRelatorio(final WsdlTestRequestStepResult runTestStep, final File file) {
        String caminho = file.getPath();
        try {
            PrintWriter arquivo = new PrintWriter(file);
            runTestStep.writeTo(arquivo);
            arquivo.close();
        } catch (IOException e) {
            Assert.fail(MensagemErro.DIRETORIO_NAO_ENCONTRADO + " " + caminho);
        } catch (Exception e) {
            Assert.fail(MensagemErro.ERRO_GENERICO + " " + e.getMessage());
        }
    }

    private static void geraRelatorioTarget(final WsdlTestRequestStepResult runTestStep, final String testStepName) {
        File file = getArquivoTarget(testStepName);
        geraRelatorio(runTestStep, file);
    }

    private static File getArquivoTarget(final String testStepName) {
        String caminhoEvidencia = "";
        try {
            caminhoEvidencia = getCaminhoEvidenciaTarget(testStepName);
            File file = new File(caminhoEvidencia);
            return file;
        } catch (IOException e) {
            Assert.fail(MensagemErro.DIRETORIO_NAO_ENCONTRADO + " " + caminhoEvidencia);
            return null;
        }
    }

    private static String getCaminhoEvidenciaTarget(final String testStepName) throws IOException {
        final String nomeArquivo = "response_" + testStepName.replace(" ", "").toUpperCase() + ".txt";
        final String caminhoEvidencia = ConfiguracaoSistema.getCaminhoEvidenciaTarget(nomeArquivo);

        criaCaminhoEvidencia(caminhoEvidencia);

        return caminhoEvidencia;
    }

}
