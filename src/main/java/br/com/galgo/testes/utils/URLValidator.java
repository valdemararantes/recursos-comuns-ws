package br.com.galgo.testes.utils;

import br.com.galgo.testes.recursos_comuns.exception.MensagemErro;
import br.com.galgo.testes.recursos_comuns.exception.URLUnavailables;
import org.junit.Assert;
import org.openqa.selenium.server.TrustEverythingSSLTrustManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by valdemar.arantes on 06/07/2015.
 */
public class URLValidator {
    private static final Logger log = LoggerFactory.getLogger(URLValidator.class);

    /**
     * Valida se é possível estabelecer uma conexão com a URL
     * @param urlStr URL a ser validada
     * @throws AssertionError Caso não seja possível estabelecer a conexão
     */
    public static void validate(String urlStr) {
        try {
            int timeout = 10_000;
            log.info("Validando a conexão com {} (Timeout utilizado: {}", urlStr, timeout);
            URL url = new URL(urlStr.trim());
            HttpsURLConnection conn = null;
            conn = (HttpsURLConnection) url.openConnection();
            conn.setConnectTimeout(timeout);
            conn.setRequestMethod("HEAD");
            TrustEverythingSSLTrustManager.trustAllSSLCertificates(conn);
            int responseCode = conn.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                log.warn("URL[{}] - responseCode: {}", urlStr, responseCode);
                log.warn("Incluindo esta URL na lista de URL inacessíveis");
                URLUnavailables.addURL(urlStr);
                Assert.fail(br.com.galgo.testes.recursos_comuns.exception.MensagemErro.URL_INACESSIVEL);
            } else {
                log.info("Conexão com {} validada com sucesso", url);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(MensagemErro.URL_INACESSIVEL);
        }
    }
}
